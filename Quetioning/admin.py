# coding: utf-8
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from .forms import UserChangeForm
from .forms import UserCreationForm
from .models import ExtUser, Period, Report

class PeriodAdmin(admin.ModelAdmin):
    # fields = ['periods_text', 'begin_date', 'end_date']
    fieldsets = [
        ('Date information', {'fields': ['begin_date', 'end_date']}),
    ]
    list_display = ('begin_date', 'end_date')
    list_filter = ['begin_date', 'end_date']
    search_fields = ['begin_date', 'end_date']

class UserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = [
        'email',
        'is_admin',
        'is_active',
        'firstname',
        'lastname',
        'middlename',
        'phone_number',
        'register_date',
    ]

    list_filter = ('is_admin',)

    fieldsets = (
                (None, {'fields': ('email', 'password')}),
                ('Персональная информация', {
                 'fields': (
                     'firstname',
                     'lastname',
                     'middlename',
                     'phone_number',
                 )}),
                ('Привелегии', {'fields': ('is_admin', 'is_active', 'is_superuser')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2'
            )}
         ),
    )

    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
    
admin.site.register(Period, PeriodAdmin)
admin.site.register(ExtUser, UserAdmin)
admin.site.register(Report)
admin.site.unregister(Group)
