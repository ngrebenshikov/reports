# coding: utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.periods, name='periods'),
    url(r'users/$', views.users, name='users'),
    url(r'holidays/$', views.holidays, name='holidays'),
    url(r'settings/$', views.settings, name='settings'),
    url(r'export/$', views.export, name='export'),
    url(r'mark/$', views.mark, name='mark'),
    url(r'report/(?P<pk>[0-9]+)/$', views.report, name='report'),
    url(r'change_rep/(?P<report_pk>[0-9]+)/$', views.change_rep, name='change_rep'),
    url(r'delete_rep/(?P<report_pk>[0-9]+)/$', views.delete_rep, name='delete_rep'),
    url(r'add_period/$', views.add_period, name='add_period'),
    url(r'change_period/(?P<pk>[0-9]+)/$', views.change_period, name='change_period'),
    url(r'delete_period/(?P<pk>[0-9]+)/$', views.delete_period, name='delete_period'),
    url(r'close_period/(?P<pk>[0-9]+)/$', views.close_period, name='close_period'),
    url(r'plus_user/(?P<pk>[0-9]+)/$', views.plus_user, name='plus_user'),
    url(r'send_notice/(?P<pk>[0-9]+)/$', views.send_notice, name='send_notice'),
    url(r'send_notices/(?P<pk>[0-9]+)/$', views.send_notices, name='send_notices'),
    url(r'add_user/$', views.add_user, name='add_user'),
    url(r'change_user/(?P<pk>[0-9]+)/$', views.change_user, name='change_user'),
    url(r'delete_user/(?P<pk>[0-9]+)/$', views.delete_user, name='delete_user'),
    url(r'month_report/$', views.month_report, name='month_report'),
    url(r'send_monthreport/(?P<year>[0-9]+)-(?P<month>[0-9]+)/$', views.send_monthreport, name='send_monthreport'),
    url(r'xml/(?P<begin>[.0-9]+)-(?P<end>[.0-9]+)-(?P<id_user>[0-9]+)/$', views.xml, name='xml'),
    url(r'zip/$', views.zip, name='zip'),
    url(r'dh/$', views.dh, name='dh'),
]