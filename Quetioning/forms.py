# coding: utf-8
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model, authenticate
from django.forms import ModelForm, TextInput, EmailInput, CheckboxInput, NumberInput, PasswordInput, DateInput, Textarea, HiddenInput, CharField, DateField
from .models import ExtUser, Period, Report
from django.shortcuts import get_object_or_404
from .models import ExtUser, Period, Report, Setting, validate_hours

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Пароль',widget=forms.PasswordInput)
    password2 = forms.CharField(label='Подтверждение пароля',widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароли не совпадают')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('email',)


class UserChangeForm(forms.ModelForm):

    password = ReadOnlyPasswordHashField(widget=forms.PasswordInput,required=False)

    def save(self, commit=True):
        user = super(UserChangeForm, self).save(commit=False)
        password = self.cleaned_data["password"]
        if password:
            user.set_password(password)
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ['email']

class LoginForm(forms.Form):
    username = forms.CharField(widget=EmailInput(attrs={'class': 'form-control','placeholder':'Email','required':'', 'autofocus':''}))
    password = forms.CharField(widget=PasswordInput(attrs={'class': 'form-control','placeholder':'Пароль','required':''}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        user = authenticate(username=username, password=password)

        if not user:
            raise forms.ValidationError('Неверный Email и/или пароль. Попробуйте еще раз.')

        if not user.is_active:
            raise forms.ValidationError('Ваша учетная запись неактивна.')
        return cleaned_data

class PeriodForm(ModelForm):
    class Meta:
        model = Period
        fields = ['begin_date', 'end_date']
        widgets = {
            'begin_date': DateInput(attrs={'class': 'form-control datepicker','placeholder':'Begin date','required':''}),
            'end_date': DateInput(attrs={'class': 'form-control datepicker','placeholder':'End date','required':''}),
        }

    def clean(self):
        cleaned_data = super(PeriodForm, self).clean()
        begin_date = cleaned_data.get("begin_date")
        end_date = cleaned_data.get("end_date")

        if begin_date and end_date:
            if begin_date >= end_date:
                raise forms.ValidationError('Дата начала должна быть меньше даты завершения!')
        return cleaned_data

class UserForm(ModelForm):
    class Meta:
        model = ExtUser
        fields = ['email', 'lastname', 'firstname', 'middlename', 'phone_number', 'last_year_holiday', 'rest', 'is_active', 'is_admin', 'send_email', 'send_sms', 'employment']
        widgets = {
            'email': EmailInput(attrs={'class': 'form-control','placeholder':'Email','required':''}),
            'firstname': TextInput(attrs={'class': 'form-control','placeholder':'Firstname','required':''}),
            'lastname': TextInput(attrs={'class': 'form-control','placeholder':'Lastname','required':''}),
            'middlename': TextInput(attrs={'class': 'form-control','placeholder':'Middlename'}),
            'is_active': CheckboxInput(attrs={'class': 'checkbox'}),
            'is_admin': CheckboxInput(attrs={'class': 'checkbox'}),
            'phone_number': NumberInput(attrs={'class': 'form-control','placeholder':'Phone number'}),
            'send_email': CheckboxInput(attrs={'class': 'checkbox', 'title': 'Извещать по Email'}),
            'send_sms': CheckboxInput(attrs={'class': 'checkbox', 'title': 'Извещать по SMS'}),
            'last_year_holiday': NumberInput(attrs={'class': 'form-control','placeholder':'Last year holiday'}),
            'rest': NumberInput(attrs={'class': 'form-control','placeholder':'Rest'}),
            'employment': DateInput(attrs={'class': 'form-control'}),
        }

class PasswordForm(forms.Form):
    password1 = forms.CharField(label = 'Введите пароль', widget=PasswordInput(attrs={'class': 'form-control','placeholder':'Password','required':''}))
    password2 = forms.CharField(label = 'Повторите пароль', widget=PasswordInput(attrs={'class': 'form-control','placeholder':'Password','required':''}))

    def clean(self):
        cleaned_data = super(PasswordForm, self).clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if password1 != password2:
            raise forms.ValidationError('Пароли не совпадают. Попробуйте еще раз.')
        return cleaned_data

class ReportForm(ModelForm):
    class Meta:
        model = Report
        fields = ['work', 'ill', 'holiday', 'mark', 'do', 'do_html']
        widgets = {
            'work': NumberInput(attrs={'class': 'form-control','placeholder':'Work','required':''}),
            'ill': NumberInput(attrs={'class': 'form-control','placeholder':'Ill','required':'', 'value':'0'}),
            'holiday': NumberInput(attrs={'class': 'form-control','placeholder':'Holiday','required':'', 'value':'0'}),
            'do': Textarea(attrs={'rows': '10', 'class': 'form-control','placeholder':'I did...'}),
            'do_html': Textarea(attrs={'rows': '10', 'class': 'form-control','placeholder':'I did...'}),
            'mark': NumberInput(attrs={'class': 'form-control','required':'', 'min':'1', 'max':'10', 'step':'1', 'value':'5'}),
        }

class SettingsForm(forms.Form):
    domen_name = forms.CharField(label = 'Домен', widget=TextInput(attrs={'class': 'form-control','required':'', 'title': 'Адрес домена будет использоваться для формировани ссылки в оповещениях по email и смс.'}))
    admin_email = forms.CharField(label = 'Email администратора', widget=EmailInput(attrs={'class': 'form-control','required':'', 'title': 'С данного адреса будут рассылаться оповещения.'}))
    relative_date = forms.CharField(label = 'Относительная дата', widget=NumberInput(attrs={'class': 'form-control','required':'', 'title': 'За сколько дней до или после начала периода его следует открыть. 0 - в тот же день, -1 - за день, 1 - на следующий день.'}))
    period_length = forms.CharField(validators=[validate_hours], label = 'Длительность периода, дни', widget=NumberInput(attrs={'class': 'form-control','required':'', 'title': 'Продолжительномть одного периода в днях.'}))
    notice_norm = forms.CharField(validators=[validate_hours], label = 'Извещение для обычных периодов', widget=NumberInput(attrs={'class': 'form-control','required':'', 'title': 'За сколько дней до окончания обычного периода автоматически высылать оповещения о необходимости заполнения.'}))
    notice_long = forms.CharField(validators=[validate_hours], label = 'Извещение для контрольных периодов', widget=NumberInput(attrs={'class': 'form-control','required':'', 'title': 'За сколько дней до окончания контрольного периода автоматически высылать оповещения о необходимости заполнения.'}))
    notice_time = forms.CharField(label = 'Время оповещения', widget=TextInput(attrs={'class': 'form-control bootstrap-timepicker timepicker input-time','required':'', 'title': 'Время автоматического оповещения о необходимости заполнения периода.'}))
    export_emails = forms.CharField(label = 'Email для контрольного отчета', widget=Textarea(attrs={'rows': '3','class': 'form-control','required':'', 'title': 'Адреса, на которые будут отправляться отчеты за месяц. Перечисляются через запятую без пробелов.'}))
    holiday_days = forms.CharField(validators=[validate_hours], label = 'Длительность отпуска, дней в год', widget=NumberInput(attrs={'class': 'form-control','required':'', 'title': 'Количество дней отпуска в год по умолчанию.'}))