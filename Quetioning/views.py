# coding: utf-8
from django.contrib import auth
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timedelta, date
from django.core.mail import send_mail
from django.template.loader import render_to_string
from urllib import request
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import serializers
import os
import zipfile
from io import BytesIO
import json
import re
from markdown import markdown
from django.utils.safestring import mark_safe

from .models import ExtUser, Period, Report, Setting
from .forms import PeriodForm, UserForm, ReportForm, PasswordForm, LoginForm, SettingsForm
from .functions import func
from django.core.exceptions import ObjectDoesNotExist
import math


def login(request):
    next_url = ''
    if request.GET:
        next_url = request.GET['next']
    print(next_url)
    login_form = LoginForm(request.POST)
    if request.method == 'POST':
        if login_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            next_ = request.POST['next']
            user = auth.authenticate(username=username, password=password)
            if user is not None and user.is_active:
                auth.login(request, user)
                if request.user.is_authenticated():
                    if next_:
                        return HttpResponseRedirect(next_)
                    elif request.user.is_admin:
                        return HttpResponseRedirect(reverse('periods'))
                    else:
                        return HttpResponseRedirect(reverse('n_main'))
            else:
                return HttpResponseRedirect(reverse('login'))
        else:
            return render(request, 'registration/login.html', {'login_form':login_form, 'next_url': next_url})
    else:
        login_form = LoginForm()
        return render(request, 'registration/login.html', {'login_form':login_form, 'next_url': next_url})

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('login'))

def user_check(user):
    return not user.is_admin

def admin_check(user):
    return user.is_admin

@login_required
@user_passes_test(user_check)
def main(request):
    user = request.user
    reports = Report.objects.filter(user=user).order_by('-period')
    form_password = PasswordForm(request.POST)
    title = 'Главная'
    if request.method == 'POST':
        if form_password.is_valid():
            user.set_password(request.POST['password1'])
            user.save()
            return HttpResponseRedirect(reverse('n_main'))
    else:
        form_password = PasswordForm()
        return render(request, 'user/main.html', {'title':title,'reports':reports, 'form_password':form_password})
    return render(request, 'user/main.html', {'title':title,'reports':reports, 'form_password':form_password})

@login_required
@user_passes_test(user_check)
def my_report(request,pk):
    user = request.user
    period = get_object_or_404(Period, pk=pk)
    title = 'Редактирование отчета'
    report = get_object_or_404(Report, user=user, period=period)
    if not period.is_close:
        #подстановка часов из xml
        if report.work == None and period.begin_date.year > 2015 and period.begin_date.month == period.end_date.month:
            report.work = func.get_workhours(period)
        if report.mark == None:
            report.mark = func.get_mark_report(report)
    #форма
    if request.method == 'POST':
        report_form = ReportForm(request.POST, instance=report)
        if report_form.is_valid():
            report_form.save()
            return HttpResponseRedirect(reverse('n_main'))
        else:
            return render(request, 'user/my_report.html', {'title':title,'report': report, 'period': period, 'report_form':report_form})
    else:
        report_form = ReportForm(instance=report)
        return render(request, 'user/my_report.html', {'title':title,'report': report, 'period': period, 'report_form':report_form})

@login_required
@user_passes_test(admin_check)
def periods(request):
    title = 'Периоды'
    #month_report
    cur_date = datetime.now()
    years = []
    if Period.objects.all():
        years = Period.objects.dates('begin_date', 'year', order="DESC")
    month_array = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    #periods
    info = []
    be_out = []
    if Period.objects.all():
        periods = Period.objects.order_by('-begin_date')
        i = 0
        for period in periods:
            info.append([])
            info[i].append(period)
            reports = Report.objects.filter(period=period)
            n = 0
            for report in reports:
                if report.work == None:
                    n=n+1
            mark = func.get_mark_period(period)
            info[i].append(mark)
            info[i].append(n)
            i += 1
    return render(request, 'admin/periods.html', {'title': title, 'month_array': month_array, 'cur_date': cur_date, 'years': years, 'info': info})

@login_required
@user_passes_test(admin_check)
def users(request):
    title = 'Сотрудники'
    staff = ExtUser.objects.all()
    users = []
    if Period.objects.all():
        periods = Period.objects.order_by('-end_date')[:5]
        i = 0
        for user in staff:
            users.append([])
            users[i].append(user)
            if user.is_admin:
                mark = None
            else:
                star = 0
                count = 0
                for period in periods:
                    if Report.objects.filter(user=user, period=period):
                        temp = get_object_or_404(Report, user=user, period=period).mark
                        if temp != None:
                            star += temp
                            count += 1
                if count > 0:
                    mark = star / count
                    mark = round(mark, 2)
                else:
                    mark = None
            users[i].append(mark)
            i += 1
    return render(request, 'admin/users.html', {'users':users, 'title': title})

@login_required
def holidays(request):
    title = 'Отпуск'
    end_of_year = False
    cur_date = datetime.now()
    year = cur_date.year
    years = []
    users = ExtUser.objects.filter(is_admin=False)
    month_array = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    if Period.objects.all():
        years = Period.objects.dates('begin_date', 'year', order="DESC")
    users = ExtUser.objects.filter(is_admin=False).order_by('lastname')
    if request.method == 'POST':
        year = int(request.POST['year'])
    info = []
    i = 0
    for user in users:
        flag = 0
        for period in Period.objects.filter(begin_date__year=year, is_close = True):
            if Report.objects.filter(user = user, period = period):
                flag = True
                break
        if flag:
            info.append([])
            info[i].append(user.get_full_name)
            rest = 0
            for month in range(1,13):
                periods = Period.objects.filter(begin_date__year=year, begin_date__month=month, is_close = True).order_by('begin_date')
                holiday = 0
                for period in periods:
                    if Report.objects.filter(user = user, period = period):
                        report = get_object_or_404(Report, user = user, period = period)
                        if report.holiday != None:
                            holiday += report.holiday
                rest += holiday
                info[i].append(holiday)
            holiday_days = int(get_object_or_404(Setting, key='holiday_days').value)
            if user.rest == None:
                user.rest = holiday_days - rest
                user.save()
            if user.last_year_holiday != None:
                holiday_days += user.last_year_holiday
            info[i].append(holiday_days)
            end_of_year_date = date(datetime.now().year, 12, 31)
            end_of_year = Period.objects.filter(begin_date__year=year, is_close = True).order_by('-end_date')[0].end_date == end_of_year_date
            if year == cur_date.year and not end_of_year:
                info[i].append(holiday_days - rest)
            else:
                info[i].append(rest)
            i += 1
    return render(request, 'admin/holidays.html', {'end_of_year': end_of_year, 'cur_date': cur_date, 'users': users,'title': title, 'years': years, 'year': year, 'month_array': month_array, 'info': info})

@login_required
def export(request):
    title = 'Экспорт'
    periods = []
    begin = date(datetime.now().year, 1, 1)
    end = date(datetime.now().year, 12, 31)
    push = False
    users = ExtUser.objects.filter(is_admin=False).order_by('lastname')
    person = False
    opened = []
    if request.method == 'POST':
        push = True
        if Period.objects.all():
            begin = datetime.strptime(request.POST['date1'], '%d.%m.%Y')
            end = datetime.strptime(request.POST['date2'], '%d.%m.%Y')
            begin = date(begin.year, begin.month, begin.day)
            end = date(end.year, end.month, end.day)
            if request.user.is_admin:
                person = get_object_or_404(ExtUser, pk=request.POST['id_user'])
            else:
                person = request.user
            i = 0
            for period in Period.objects.order_by('begin_date'):
                if (period.begin_date >= begin and period.end_date <= end) or (period.begin_date < begin and period.end_date < end and period.end_date > begin) or (period.begin_date >= begin and period.begin_date < end and period.end_date >= end) or (period.begin_date <= begin and period.end_date >= end):
                    if Report.objects.filter(period=period, user=person):
                        periods.append([])
                        periods[i].append(period)
                        periods[i].append(get_object_or_404(Report, user=person, period=period).do)
                        if not period.is_close:
                            opened.append(period)
                        i += 1
    begin = datetime.strftime(begin, '%d.%m.%Y')
    end = datetime.strftime(end, '%d.%m.%Y')
    return render(request, 'admin/export.html', {'title': title, 'periods': periods, 'begin': begin, 'end':end, 'push': push, 'users': users, 'person': person, 'opened': opened})

@login_required
@user_passes_test(admin_check)
def settings(request):
    title="Настройки"
    #создаем настройки, если их нет
    if not Setting.objects.all():
        Setting(key='domen_name', value='http://127.0.0.1:8000').save()
        Setting(key='relative_date', value='0').save()
        Setting(key='period_length', value='7').save()
        Setting(key='notice_norm', value='2').save()
        Setting(key='notice_long', value='5').save()
        Setting(key='admin_email', value='area9abakan@gmail.com').save()
        Setting(key='notice_time', value='10:00').save()
        Setting(key='export_emails', value='grebenshikov.n@gmail.com, ViktoriLisenok@yandex.ru').save()
        Setting(key='holiday_days', value='20').save()
    #получаем значения настроек
    domen = get_object_or_404(Setting, key='domen_name').value
    relative_date = get_object_or_404(Setting, key='relative_date').value
    period_length = get_object_or_404(Setting, key='period_length').value
    notice_norm = get_object_or_404(Setting, key='notice_norm').value
    notice_long = get_object_or_404(Setting, key='notice_long').value
    notice_time = get_object_or_404(Setting, key='notice_time').value
    admin_email = get_object_or_404(Setting, key='admin_email').value
    export_emails = get_object_or_404(Setting, key='export_emails').value
    holiday_days = get_object_or_404(Setting, key='holiday_days').value
    if request.method == 'POST':
        form_setting = SettingsForm(request.POST)
        if form_setting.is_valid():
            '''export_emails = Setting(
                key='export_emails',
                value='grebenshikov.n@gmail.com, ViktoriLisenok@yandex.ru, 89233172383@ya.ru'
            )
            export_emails.save()'''
            #сохраняем настройки
            func.save_setting(request, 'domen_name')
            func.save_setting(request, 'admin_email')
            func.save_setting(request, 'relative_date')
            func.save_setting(request, 'period_length')
            func.save_setting(request, 'notice_norm')
            func.save_setting(request, 'notice_long')
            func.save_setting(request, 'notice_time')
            func.save_setting(request, 'export_emails')
            func.save_setting(request, 'holiday_days')
            return HttpResponseRedirect(reverse('settings'))
    else:
        #заполняем форму
        form_setting = SettingsForm({'domen_name': domen, 'admin_email': admin_email, 'relative_date': relative_date, 'period_length': period_length, 'notice_norm': notice_norm, 'notice_long': notice_long, 'notice_time':notice_time, 'export_emails': export_emails, 'holiday_days': holiday_days })
    return render(request, 'admin/settings.html', {'form_setting': form_setting, 'title': title})

@login_required
@user_passes_test(admin_check)
def report(request, pk):
    block_title = 'Периоды'
    block_url = reverse('periods')
    title = 'Просмотр отчета'
    period = get_object_or_404(Period, pk=pk)
    reports = Report.objects.filter(period=pk)
    users = ExtUser.objects.filter(is_active=True, is_admin=False)
    array = []
    add_users = []
    for report in reports:
        array.append(report.user)
    for user in users:
        if user not in array:
            add_users.append(user)
    return render(request, 'admin/report.html', {'block_url': block_url, 'block_title': block_title, 'add_users': add_users, 'title': title, 'reports': reports, 'period': period})

@login_required
@user_passes_test(admin_check)
def add_period(request):
    block_title = 'Периоды'
    block_url = reverse('periods')
    title = 'Добавление периода'
    if request.method == 'POST':
        if Period.objects.all():
            end = Period.objects.order_by('-end_date')[0].end_date
            per = Period(
                begin_date= end + timedelta(days=1),
                end_date= end + timedelta(days=7),
            )
            form_period = PeriodForm(request.POST,instance=per)
        else:
            form_period = PeriodForm(request.POST)
        if form_period.is_valid():
            period = form_period.save()
            func.create_reports(period)
            return HttpResponseRedirect(reverse('periods'))
        else:
            if Period.objects.all():
                form_period = PeriodForm(instance=per)
            else:
                form_period = PeriodForm()
            return render(request, 'admin/add_period.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form_period':form_period})
    else:
        form_period = PeriodForm()
        return render(request, 'admin/add_period.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form_period':form_period})

@login_required
@user_passes_test(admin_check)
def change_period(request, pk):
    '''start = datetime.strptime(str(period.begin_date), "%Y-%m-%d").strftime('%d.%m.%Y')'''
    block_title = 'Периоды'
    block_url = reverse('periods')
    title = 'Редактирование периода'
    period = get_object_or_404(Period, pk=pk)
    close1 = period.is_close
    print ('Close1: ', close1)
    if request.method == 'POST':
        form = PeriodForm(request.POST, instance=period)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('periods'))
        else:
            form = PeriodForm(instance=period)
            return render(request, 'admin/change_period.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form': form})
    else:
        form = PeriodForm(instance=period)
        return render(request, 'admin/change_period.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form': form})

@login_required
@user_passes_test(admin_check)
def delete_period(request, pk):
    if request.method == 'POST':
        period = get_object_or_404(Period, pk=pk)
        period.delete()
    return HttpResponseRedirect(reverse('periods'))

@login_required
@user_passes_test(admin_check)
def close_period(request, pk):
    if request.method == 'POST':
        period = get_object_or_404(Period, pk=pk)
        cur_date = datetime.now()
        # открытие периода
        holiday_days = int(get_object_or_404(Setting, key='holiday_days').value)
        if period.is_close:
            period.is_close = False
            period.save()
            if period.begin_date.year == cur_date.year or (cur_date.month == 1 and period.end_date.day == 31 and period.end_date.month == 12 and period.end_date.year == cur_date.year - 1):
                reports = Report.objects.filter(period=period)
                for report in reports:
                    # остаток равен None или конец года
                    if report.user.rest == None or (period.end_date.day == 31 and period.end_date.month == 12 and (period.end_date.year == cur_date.year or period.end_date.year == cur_date.year - 1)):
                        if period.end_date.day == 31 and period.end_date.month == 12 and (period.end_date.year == cur_date.year or period.end_date.year == cur_date.year - 1):
                            report.user.last_year_holiday = 0
                            report.user.save()
                        holiday = 0
                        for per in Period.objects.filter(begin_date__year=cur_date.year, is_close = True):
                            if Report.objects.filter(user = report.user, period = per):
                                report = Report.objects.filter(user = report.user, period = per)[0]
                                if report.holiday != None:
                                    holiday += report.holiday
                        report.user.rest = holiday_days - holiday
                        report.user.save()
                    else:
                        rest = report.holiday if report.holiday != None else 0
                        #изменение остатков отпуска
                        if rest > 0:
                            if report.user.rest == holiday_days and rest <= 5:
                                report.user.last_year_holiday = rest
                            else:
                                flag = rest + report.user.rest - holiday_days
                                if flag <= 0:
                                    report.user.rest += rest
                                else:
                                    report.user.rest = holiday_days
                                    if rest <= 5:
                                        report.user.last_year_holiday = rest
                                    else:
                                        report.user.last_year_holiday = 5
                        report.user.save()
        # закрытие периода
        else:
            period.is_close = True
            period.save()
            if period.begin_date.year == cur_date.year:
                reports = Report.objects.filter(period_id=period)
                for report in reports:
                    cur_date = datetime.now()
                    # остаток равен None
                    rest = report.holiday if report.holiday != None else 0
                    if report.user.rest == None:
                        holiday = 0
                        for per in Period.objects.filter(begin_date__year=cur_date.year, is_close = True):
                            if Report.objects.filter(user = report.user, period = per):
                                report = Report.objects.filter(user = report.user, period = per)[0]
                                if report.holiday != None:
                                    holiday += report.holiday
                        report.user.rest = holiday_days - holiday
                        if period.end_date.day == 31 and period.end_date.month == 12 and (period.end_date.year == cur_date.year or period.end_date.year == cur_date.year - 1):
                            if report.user.rest > 0:
                                report.user.rest -= rest
                                if report.user.rest > 5:
                                    report.user.last_year_holiday = 5
                                else:
                                    report.user.last_year_holiday = report.user.rest
                            report.user.rest = holiday_days
                    else:
                        # конец года
                        if period.end_date.day == 31 and period.end_date.month == 12 and (period.end_date.year == cur_date.year or period.end_date.year == cur_date.year - 1):
                            holiday_days = int(get_object_or_404(Setting, key='holiday_days').value)
                            if report.user.rest > 0:
                                report.user.rest -= rest
                                if report.user.rest > 5:
                                    report.user.last_year_holiday = 5
                                else:
                                    report.user.last_year_holiday = report.user.rest
                            report.user.rest = holiday_days
                        else:
                            #изменение остатков отпуска
                            if (report.user.last_year_holiday > 0 and rest > 0):
                                if report.user.last_year_holiday - rest > 0:
                                    report.user.last_year_holiday -= report.holiday
                                    rest = 0
                                else:
                                    rest = rest - report.user.last_year_holiday
                                    report.user.last_year_holiday = 0
                            report.user.rest -= rest
                            # конец квартала
                            if period.end_date.day == 31 and period.end_date.month == 3 and period.end_date.year == cur_date.year:
                                report.user.last_year_holiday = 0
                    report.user.save()
    return HttpResponseRedirect(reverse('periods'))

@login_required
@user_passes_test(admin_check)
def change_rep(request,report_pk):
    block_title = 'Периоды'
    block_url = reverse('periods')
    report = get_object_or_404(Report, pk=report_pk)
    title = 'Редактирование отчета'
    if report.work == None and report.period.begin_date.year > 2015 and report.period.begin_date.month == report.period.end_date.month:
        report.work = func.get_workhours(report.period)
    if report.mark == None:
        report.mark = func.get_mark_report(report)
    if request.method == 'POST':
        report_form = ReportForm(request.POST, instance=report)
        if report_form.is_valid():
            report_form.save()
            return HttpResponseRedirect(reverse('report', args=[report.period.pk]))
        else:
            return render(request, 'admin/change_rep.html', {'block_url': block_url, 'block_title': block_title,'title':title,'report': report, 'report_form':report_form})
    else:
        report_form = ReportForm(instance=report)
        return render(request, 'admin/change_rep.html', {'block_url': block_url, 'block_title': block_title,'title':title,'report': report, 'report_form':report_form})

@login_required
@user_passes_test(admin_check)
def delete_rep(request, report_pk):
    if request.method == 'POST':
        report = get_object_or_404(Report, pk=report_pk)
        period = report.period.pk
        report.delete()
    return HttpResponseRedirect(reverse('report', args=[period]))

@login_required
@user_passes_test(admin_check)
def plus_user(request, pk): # добавление пользователя в открытый период
    if request.method == 'POST':
        period = get_object_or_404(Period, pk=pk)
        user = get_object_or_404(ExtUser, pk=request.POST['id_user'])
        if period and user:
            report = Report(
                user=user,
                period=period
            )
            report.save()
        return HttpResponseRedirect(reverse('report', args=[pk]))

@login_required
@user_passes_test(admin_check)
def send_notice(req, pk):
    if req.method == 'POST':
        report = get_object_or_404(Report, pk=pk)
        func.notice(report)
    return HttpResponseRedirect(reverse('report', args=[report.period.pk]))

@login_required
@user_passes_test(admin_check)
def send_notices(req, pk):
    if req.method == 'POST':
        period = get_object_or_404(Period, pk=pk)
        func.notices(period)
    return HttpResponseRedirect(reverse('report', args=[pk]))

@login_required
@user_passes_test(admin_check)
def add_user(request):
    block_title = 'Сотрудники'
    block_url = reverse('users')
    title = 'Добавление сотрудника'
    form_user = UserForm(request.POST)
    if request.method == 'POST':
        if form_user.is_valid():
            user = form_user.save()
            user.set_password('1')
            user.save()
            return HttpResponseRedirect(reverse('users'))
    return render(request, 'admin/add_user.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form_user':form_user})

@login_required
@user_passes_test(admin_check)
def change_user(request, pk):
    block_title = 'Сотрудники'
    block_url = reverse('users')
    title = 'Редактирование сотрудника'
    user = get_object_or_404(ExtUser, pk=pk)
    form_password = PasswordForm(request.POST)
    form_user = UserForm(request.POST, instance=user)
    if request.method == 'POST':
        if form_password.is_valid():
            user.set_password(request.POST['password1'])
            user.save()
            return HttpResponseRedirect(reverse('users'))
        if form_user.is_valid():
            form_user.save()
            return HttpResponseRedirect(reverse('users'))
        else:
            user = get_object_or_404(ExtUser, pk=pk)
            form_user = UserForm(instance=user)
            return render(request, 'admin/change_user.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form_user': form_user, 'form_password': form_password})
    else:
        form_user = UserForm()
        form_password = PasswordForm()
        return render(request, 'admin/change_user.html', {'block_url': block_url, 'block_title': block_title,'title': title, 'form_user': form_user, 'form_password': form_password})

@login_required
@user_passes_test(admin_check)
def delete_user(request, pk):
    if request.method == 'POST':
        user = get_object_or_404(ExtUser, pk=pk)
        user.delete()
    return HttpResponseRedirect(reverse('users'))

@login_required
@user_passes_test(admin_check)
def month_report(request):
    if request.method == 'POST':
        block_title = 'Периоды'
        block_url = reverse('periods')
        title = 'Отчет за месяц'
        info = []
        opened = []
        month = int(request.POST['month'])
        year = int(request.POST['year'])
        periods = Period.objects.filter(begin_date__year=year, begin_date__month=month).order_by('begin_date')
        users = ExtUser.objects.filter(is_admin=False)
        if periods and users:
            info = func.get_info(users, periods)
        for period in periods:
            if not period.is_close:
                opened.append(period)
        return render(request, 'admin/month_report.html', {'block_url': block_url, 'block_title': block_title, 'title':title, 'year':year, 'month':month, 'periods': periods, 'info': info, 'opened': opened})
    else:
        return HttpResponseRedirect(reverse('periods'))

@login_required
@user_passes_test(admin_check)
def send_monthreport(request, year, month):
    if request.method == 'POST':
        periods = Period.objects.filter(begin_date__year=year, begin_date__month=month).order_by('begin_date')
        users = ExtUser.objects.filter(is_admin=False)
        if periods and users:
            info = func.get_info(users, periods)
            admin_email = get_object_or_404(Setting, key='admin_email').value
            export_emails = get_object_or_404(Setting, key='export_emails').value
            emails = export_emails.split(',')
            message = render_to_string('admin/month_report.txt', {'year':year, 'month':month, 'info': info})
            send_mail('Отчет за месяц', message, admin_email, emails, fail_silently=False)
    return HttpResponseRedirect(reverse('periods'))

@login_required
def xml(request, begin, end, id_user):
    periods = []
    begin = datetime.strptime(begin, '%d.%m.%Y')
    begin = date(begin.year, begin.month, begin.day)
    end = datetime.strptime(end, '%d.%m.%Y')
    end = date(end.year, end.month, end.day)
    user = get_object_or_404(ExtUser, pk=id_user)
    i = 0
    if Period.objects.all():
        for period in Period.objects.order_by('begin_date'):
            if (period.begin_date >= begin and period.end_date <= end) or (period.begin_date < begin and period.end_date < end and period.end_date > begin) or (period.begin_date >= begin and period.begin_date < end and period.end_date >= end) or (period.begin_date <= begin and period.end_date >= end):
                if Report.objects.filter(period=period, user=user):
                    report = get_object_or_404(Report, period=period, user=user)
                    task1 = '<task from="' + datetime.strftime(period.begin_date, '%d.%m.%Y') + '" to="' + datetime.strftime(period.end_date, '%d.%m.%Y') + '">'
                    periods.append([])
                    periods[i].append(period)
                    do = ''
                    if (report.do):
                        if (report.do_html):
                            do = report.do_html
                        else:
                            do = markdown(report.do)
                        if do.find('<em>') >= 0:
                            do = do.replace('<em>', '')
                            do = do.replace('</em>', '')
                        if do.find('<strong>') >= 0:
                            do = do.replace('<strong>', '')
                            do = do.replace('</strong>', '')
                        if do.find('<p>') >= 0:
                            do = do.replace('<p>', '<li>')
                            do = do.replace('</p>', '</li>')
                        #do = re.sub(r'</?[^(task)(text)]>', '', do)
                        if 2 > do.find('<ul>') >= 0:
                            part = do.partition('<ul>')
                            do = part[0] + part[2]
                            part = do.rpartition('</ul>')
                            do = part[0] + part[2]
                        do = do.replace('<ul>', '<task>')
                        do = do.replace('</ul>', '</task>')
                        do = do.replace('<li>', '<text>')
                        do = do.replace('</li>', '')
                        #do = do.replace('</text>', '')
                        pos = 0
                        for j in range(do.count('<text>')):
                            pos = do.find('<', do.find('<text>', pos)+5);
                            if pos == -1:
                                do = do + '</text>'
                            else:
                                do = do[:pos] + '</text>' + do[pos:]
                    do = task1 + do + '</task>'
                    do = func.get_xml_lvl1(do, task1)
                    periods[i].append(mark_safe(do))
                    i += 1
    data = render_to_string('admin/xml.txt', {'periods': periods, 'user': user})
    response = HttpResponse(data, content_type='application/xml')
    content = 'attachment; filename=monthreport-'+ user.email + '-' + str(begin) + '-' + str(end) + '.xml'
    response['Content-Disposition'] = content
    return response

@login_required
@user_passes_test(admin_check)
def zip(request):
    begin = datetime.strptime(request.POST['date_one'], '%d.%m.%Y')
    begin = date(begin.year, begin.month, begin.day)
    end = datetime.strptime(request.POST['date_two'], '%d.%m.%Y')
    end = date(end.year, end.month, end.day)
    zip_filename = 'workreport-' + str(begin) + '-' + str(end) + '.zip'
    buff = BytesIO()
    zf = zipfile.ZipFile(buff, mode="w")
    users = ExtUser.objects.filter(is_admin=False)
    for user in users:
        periods = []
        i = 0
        if Period.objects.all():
            for period in Period.objects.order_by('begin_date'):
                if (period.begin_date >= begin and period.end_date <= end) or (period.begin_date < begin and period.end_date < end and period.end_date > begin) or (period.begin_date >= begin and period.begin_date < end and period.end_date >= end) or (period.begin_date <= begin and period.end_date >= end):
                    if Report.objects.filter(period=period, user=user):
                        periods.append([])
                        periods[i].append(period)
                        periods[i].append(get_object_or_404(Report, user=user, period=period).do)
                        i += 1
        data = render_to_string('admin/xml.txt', {'periods': periods, 'user': user})
        filename = user.email + '-' + str(begin) + '-' + str(end) + '.xml'
        zf.writestr(filename, data.encode('utf-8'))
    zf.close()
    resp = HttpResponse(buff.getvalue(), content_type = "application/x-zip-compressed")
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
    return resp

@login_required
@user_passes_test(admin_check)
def mark(request):
    title = "Оценка"
    begin = date(datetime.now().year, 1, 1)
    end = date(datetime.now().year, 12, 31)
    push = False
    periods = False
    users = ExtUser.objects.filter(is_admin=False).order_by('lastname')
    person = False
    opened = []
    marks = []
    if request.method == 'POST':
        push = True
        if Period.objects.all():
            periods = True
            begin = datetime.strptime(request.POST['date1'], '%d.%m.%Y')
            end = datetime.strptime(request.POST['date2'], '%d.%m.%Y')
            begin = date(begin.year, begin.month, begin.day)
            end = date(end.year, end.month, end.day)
            if request.POST['id_user'] != "all":
                person = get_object_or_404(ExtUser, pk=request.POST['id_user'])
            for period in Period.objects.order_by('begin_date'):
                if (period.begin_date >= begin and period.end_date <= end) or (period.begin_date < begin and period.end_date < end and period.end_date > begin) or (period.begin_date >= begin and period.begin_date < end and period.end_date >= end) or (period.begin_date <= begin and period.end_date >= end):
                    if not period.is_close:
                        opened.append(period)
                    mark = 0
                    if person:
                        if Report.objects.filter(period=period, user=person):
                            mark = get_object_or_404(Report, user=person, period=period).mark
                            if mark == None:
                                mark = 0
                    else:
                        mark = func.get_mark_period(period)
                    marks.append({'begin_date': str(period.begin_date), 'end_date': str(period.end_date), 'mark': mark})
            print (marks)
    mas = json.dumps(marks)
    print(mas)
    begin = datetime.strftime(begin, '%d.%m.%Y')
    end = datetime.strftime(end, '%d.%m.%Y')
    return render(request, 'admin/mark.html', {'mas': mas, 'title': title, 'periods': periods, 'begin': begin, 'end':end, 'push': push, 'users': users, 'person': person, 'opened': opened})


def dh(request):

    years = []
    rest = 0
    rest_year_now = 0
    year = datetime.now().year
    fio = None
    rests = None
    holiday_days = int(Setting.objects.all().get(id=9).value)
    year_periods = []
    users_info =[]

    users = ExtUser.objects.filter(is_admin=False)
    if Period.objects.all():
        years = Period.objects.dates('begin_date', 'year', order="DESC")
    if request.method == 'POST':
        year = int(request.POST['year'])


    for user in users:

        rests = 0
        year_periods = []
        rest_year_now = 0
        user_info = []
        fio = user.get_full_name()

        # расчет отпуска за первый период
        start_job = user.employment
        #first_holiday = ((12 - start_job.month) * 20)//12
        first_holiday = int(math.ceil((date(start_job.year+1, 1, 1) - start_job).days/365*20))

        # расчет остатка отпуска за первый период
        reports = Report.objects.filter(user_id=user.id)#выбранны все отчеты конкретного пользователя
        periods = Period.objects.filter(begin_date__year=start_job.year).order_by('begin_date')
        sum_holiday = 0
        for month in periods:
            try:
                report = reports.get(period_id=month.id)
            except ObjectDoesNotExist:
                sum_holiday = 0
            else:
                sum_holiday += report.holiday
        #надо привести к одной единици измерения или дни итли часы
        sum_holiday = sum_holiday//8


        rest = first_holiday - sum_holiday
        if rest > 5:
            rest = 5
        if start_job.year >= year:
            rest = 0

        # расчет остатка за прошедшие года
        for y in range(start_job.year+1, year):
            periods = Period.objects.filter(begin_date__year=y).order_by('begin_date')
            for month in periods:
                try:
                    report = reports.get(period_id=month.id)
                except ObjectDoesNotExist:
                    rest = 0
                else:
                    rest = rest - report.holiday//8
                if month.begin_date.month == 3 and rest > 0:
                    rest = 0
            rest = rest + holiday_days
            if rest > 5:
                rest = 5
        rests = rest
        #расчет отпуска за текуший период
        if year == datetime.now().year:
            periods = Period.objects.filter(begin_date__year=datetime.now().year).order_by('begin_date')
            for month in periods:
                try:
                    report = reports.get(period_id=month.id)
                except ObjectDoesNotExist:
                    rest = 0
                else:
                    rest = rest - report.holiday//8
                rest_year_now = holiday_days +rest
                if month.begin_date.month == 3:
                    if rest > 0:
                        rest = 0
        periods = Period.objects.filter(begin_date__year=year).order_by('begin_date')
        for month in periods:
            try:
                report = reports.get(period_id=month.id)
            except ObjectDoesNotExist:
                year_periods.append(0)
            else:
                year_periods.append(report.holiday)
        while len(year_periods) < 12:
           year_periods.append(0)
        #отпуск зануляется если сотрудник не работал
        if start_job.year > year:
            holiday_days = 0
        #расчут отпуска за первый год работы
        if start_job.year == year:
            holiday_days = first_holiday
            rests = 0
            rest_year_now = 0
            for month in periods:
                try:
                    rest_year_now += reports.get(period_id=month.id).holiday
                except ObjectDoesNotExist:
                    rest_year_now = 0
            rest_year_now = rest_year_now//8
        #Расчет отпуска за предшествующие года кроме текущего
        if start_job.year != year and year != datetime.now().year:
            rest_year_now = 0
            for month in periods:
                try:
                    rest_year_now += reports.get(period_id=month.id).holiday
                except ObjectDoesNotExist:
                    rest_year_now = 0
            rest_year_now = rest_year_now//8
        #формирую структуру для выброски в шаблон
        user_info.append(fio)
        user_info.append(rests)
        user_info.append(holiday_days)
        user_info.append(year_periods)
        user_info.append(rest_year_now)

        users_info.append(user_info)



    return render(request, 'admin/dh.html', {'years': years,'year': year,
    'users_info': users_info})



