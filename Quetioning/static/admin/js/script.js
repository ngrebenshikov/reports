$(function(){
    $("table:not(.period, .main)").dataTable({"iDisplayLength": 25});
    $("table.period, table.main").dataTable({
      bSort: false,
      "iDisplayLength": 25
    });

    $.fn.datepicker.dates['ru'] = {
      days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
      daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
      daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
      months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
      today: "Сегодня",
      clear: "Очистить",
      format: "dd.mm.yyyy",
      weekStart: 1
    };

    if ($.isFunction($.fn.datepicker)) {
      $(".datepicker").each(function(i, el) {
        var $this = $(el),
          opts = {
            language: 'ru',
            format: 'dd.mm.yyyy',
            //format: 'yyyy-mm-dd',
            startDate: '',
            endDate: '',
            daysOfWeekDisabled: '',
            startView: 0,
            autoclose: true
          },
          $n = $this.next(),
          $p = $this.prev();

        $this.datepicker(opts);

        if ($n.is('.input-group-addon') && $n.has('a')) {
          $n.on('click', function(ev) {
            ev.preventDefault();

            $this.datepicker('show');
          });
        }

        if ($p.is('.input-group-addon') && $p.has('a')) {
          $p.on('click', function(ev) {
            ev.preventDefault();

            $this.datepicker('show');
          });
        }
      });
    }

    $('input.input-time').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: true
    });
    
    /*$('input.input-slider').slider({
      tooltip: 'always',
      formatter: function(value) {
        return value;
      }
    });*/

    /*$('a[data-toggle="tab"]').on('shown.bs.tab', function () {
      // сохраним последнюю вкладку
      localStorage.setItem('lastTab', $(this).attr('href'));
    });

    //перейти к последней вкладке, если она существует:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
      $('a[href=' + lastTab + ']').tab('show');
    }
    else{
      // установить первую вкладку активной если lasttab не существует
      $('a[data-toggle="tab"]:first').tab('show');
    }*/
  });