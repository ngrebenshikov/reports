from django.shortcuts import get_object_or_404
from datetime import date, datetime, timedelta
from .models import ExtUser, Period, Report, Setting
from calendar import monthrange
from django.core.mail import send_mail
from django.template.loader import render_to_string
from urllib import request
from lxml import etree
import re

class func:
    def notices(period): # оповещение всех не заполнивших по кнопке
        domen = get_object_or_404(Setting, key='domen_name').value
        reports = Report.objects.filter(period=period)
        admin_email = get_object_or_404(Setting, key='admin_email').value
        for report in reports:
            if report.work == None:
                message = render_to_string('admin/email.txt', {'report': report, 'domen': domen})
                if report.user.send_email:
                    if send_mail('Еженедельная анкета', message, admin_email, [report.user.email], fail_silently=False):
                        report.last_notice = datetime.today()
                        report.save()
                if report.user.send_sms:
                    sms = render_to_string('admin/sms.txt', {'report': report, 'domen': domen})
                    sms.replace(' ', '%20')
                    if request.urlopen(sms):
                        report.last_notice = datetime.today()
                        report.save()

    def notice(report): # оповещение сотрудника по кнопке
        domen = get_object_or_404(Setting, key='domen_name').value
        admin_email = get_object_or_404(Setting, key='admin_email').value
        if report.user.send_email:
            message = render_to_string('admin/email.txt', {'report': report, 'domen': domen})
            if send_mail('Еженедельная анкета', message, admin_email, [report.user.email], fail_silently=False):
                report.last_notice = datetime.today()
                report.save()
        if report.user.send_sms:
            sms = render_to_string('admin/sms.txt', {'report': report, 'domen': domen})
            sms.replace(' ', '%20')
            if request.urlopen(sms):
                report.last_notice = datetime.today()
                report.save()

    def create_reports(period): # создание отчетов при открытии периода
        users = ExtUser.objects.filter(is_active=True, is_admin=False)
        for user in users:
            report = Report(
                user=user,
                period=period
            )
            report.save()

    def auto_notices(period, notice_): # автоматическое оповещение всех не заполнивших
        notice = date(notice_.year, notice_.month, notice_.day)
        reports = Report.objects.filter(period=period)
        for report in reports:
            if (not report.last_notice or report.last_notice < notice) and (report.work == None):
                func.notice(report)

    def report_email(period): # email-отчет администратору о не заполнивших период
        domen = get_object_or_404(Setting, key='domen_name').value
        admin_email = get_object_or_404(Setting, key='admin_email').value
        admins = ExtUser.objects.filter(is_admin=True, is_active=True)
        if admins:
            unfilled = []
            reports = Report.objects.filter(period=period)
            for report in reports:
                if report.work == None:
                    unfilled.append(report.user)
            for admin in admins:
                if admin.send_email:
                    message = render_to_string('admin/report_email.txt', {'admin': admin, 'domen': domen, 'period': period, 'unfilled': unfilled})
                    if send_mail('Отчет о периоде', message, admin_email, [admin.email], fail_silently=False):
                        period.reported = True
                        period.save()

    def save_setting(request, key): # сохранение настройки
        setting = get_object_or_404(Setting, key=key)
        setting.value = request.POST[key]
        setting.save()

    def get_info(users, periods): # получение массива для отчета за месяц (по часам)
        info = []
        i = 0
        for user in users:
            flag = 0
            for period in periods:
                if Report.objects.filter(user = user, period = period):
                    flag = True
                    break
            if flag:
                info.append([])
                info[i].append(user.get_full_name)
                work = 0
                ill = 0
                holiday = 0
                for period in periods:
                    if Report.objects.filter(user = user, period = period):
                        report = get_object_or_404(Report, user = user, period = period)
                        if report.work != None:
                            work += report.work
                        if report.ill != None:
                            ill += report.ill
                        if report.holiday != None:
                            holiday += report.holiday
                info[i].append(work)
                info[i].append(ill)
                info[i].append(holiday)
                i += 1
        return info

    def get_info_do(begin, end, user): # получение массива для отчета за период (по сделанному)
        periods = []
        i = 0
        for period in Period.objects.order_by('begin_date'):
            if (period.begin_date >= begin and period.end_date <= end) or (period.begin_date < begin and period.end_date < end and period.end_date > begin) or (period.begin_date >= begin and period.begin_date < end and period.end_date >= end) or (period.begin_date <= begin and period.end_date >= end):
                if Report.objects.filter(period=period, user=user):
                    periods.append([])
                    periods[i].append(period)
                    periods[i].append(get_object_or_404(Report, user=user, period=period).do)
                    i += 1
        return periods

    def get_workhours(period): # получение часов для подстановки в анкету
        parser = etree.XMLParser(encoding='utf-8')
        xml = 'templates/admin/calendar/' + str(period.begin_date.year) + '.xml'
        root = etree.parse(xml, parser)
        for child_of_root in root.iter('month'):
            if child_of_root.items()[0][1] == str(period.begin_date.month):
                print (child_of_root.items())
                days = []
                for item in child_of_root.iterfind('.//'):
                    days.append(int(item.text))
                relax = 0
                for day in days:
                    if period.begin_date.day <= day <= period.end_date.day:
                        relax += 1
                    elif day > period.end_date.day:
                        break
                break
        workhours = ((period.end_date - period.begin_date).days + 1 - relax) * 8
        return workhours

    def get_mark_period(period): # средняя оценка за период
        star = 0
        count = 0
        reports = Report.objects.filter(period=period)
        for report in reports:
            if report.mark:
                star += report.mark
                count += 1
        if count > 0:
            mark = star / count
            mark = round(mark, 2)
        else:
            mark = None
        return mark

    def get_mark_report(report):
        mark = None
        date = report.period.begin_date + timedelta(days=-1)
        while not Period.objects.filter(end_date=date):
            date += timedelta(days=-1)
        prev_per = Period.objects.filter(end_date=date).order_by('-pk')[0]
        if Report.objects.filter(user=report.user, period=prev_per):
            mark = get_object_or_404(Report, user=report.user, period=prev_per).mark
        if mark == None:
            mark = 5
        return mark

    def get_xml_lvl_other(do, pos):
        pos = do.find('</text>', pos);
        if pos == -1:
            return do
        else:
            pos += 6
            if pos+2 > do.find('</task>', pos) >= 0:
                return do;
            elif pos+2 > do.find('<text>', pos) >= 0:
                pos = do.find('<text>', pos)
                do = do[:pos] + '</task><task>' + do[pos:]
                pos += 6
                do = func.get_xml_lvl_other(do, pos)
            elif pos+2 > do.find('<task>', pos) >= 0:
                do = func.get_xml_lvl_other(do, pos)
                k = 0
                while True:
                    pattern = re.compile(r'<task>|</task>')
                    res = pattern.search(do, pos)
                    if res.group(0) == '<task>':
                        k += 1
                    elif res.group(0) == '</task>':
                        k -= 1
                    else:
                        k = 0
                    pos = res.end()
                    if not (pos+6 > do.find('<task>', pos) >= 0) and k == 0:
                        break
                if pos+6 >= do.find('<text>', pos) >= 0:
                    pos = do.find('<text>', pos)
                    do = do[:pos] + '</task><task>' + do[pos:]
                    pos += 6
                    do = func.get_xml_lvl_other(do, pos)
        return do

    def get_xml_lvl1(do, task1, pos=0):
        pos = do.find('</text>', pos);
        if pos == -1:
            return do
        else:
            pos += 6
            if pos+5 > do.find('</task>', pos) >= 0:
                return do;
            elif pos+2 > do.find('<text>', pos) >= 0:
                pos = do.find('<text>', pos)
                do = do[:pos] + '</task>' + task1 + do[pos:]
                pos += 6
                do = func.get_xml_lvl1(do, task1, pos)
            elif pos+2 > do.find('<task>', pos) >= 0:
                #length = len(do)
                do = func.get_xml_lvl_other(do, pos)
                #pos = pos + (len(do) - length)
                k = 0
                while True:
                    pattern = re.compile(r'<task>|</task>')
                    res = pattern.search(do, pos)
                    if res.group(0) == '<task>':
                        k += 1
                    elif res.group(0) == '</task>':
                        k -= 1
                    else:
                        k = 0
                    pos = res.end()
                    if not (pos+6 > do.find('<task>', pos) >= 0) and k == 0:
                        break
                if pos+6 >= do.find('<text>', pos) >= 0:
                    pos = do.find('<text>', pos)
                    do = do[:pos] + '</task>' + task1 + do[pos:]
                    pos += 6
                    do = func.get_xml_lvl1(do, task1, pos)
        return do