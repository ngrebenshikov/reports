# coding: utf-8
from django.db import models
from django import forms
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
import datetime
from django.utils import timezone
from django.core.exceptions import NON_FIELD_ERRORS
# Create your models here.

#validators
def validate_hours(value):
    if int(value) < 0:
        raise forms.ValidationError('Значение должно быть неотрицательным!')

class UserManager(BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Email error')

        ExtUser = self.model(
            email=UserManager.normalize_email(email),
        )

        ExtUser.set_password(password)
        ExtUser.save(using=self._db)
        return ExtUser

    def create_superuser(self, email, password):
        ExtUser = self.create_user(email, password)
        ExtUser.is_admin = True
        ExtUser.is_superuser = True
        ExtUser.save(using=self._db)
        return ExtUser


class ExtUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField('Email', max_length=255, unique=True, db_index=True)
    firstname = models.CharField('Имя', max_length=40, null=True, blank=True)
    lastname = models.CharField('Фамилия', max_length=40, null=True, blank=True)
    middlename = models.CharField('Отчество', max_length=40, null=True, blank=True)
    register_date = models.DateField('Дата регистрации', auto_now_add=True)
    is_active = models.BooleanField('Активность', default=True)
    is_admin = models.BooleanField('Администратор', default=False)
    phone_number = models.CharField('Номер телефона', max_length=40, null=True, blank=True)
    send_email = models.BooleanField('Email', default=True)
    send_sms = models.BooleanField('SMS', default=True)
    last_year_holiday = models.IntegerField('Остаток отпуска c прошлых лет, дни', null=True, blank=True, validators=[validate_hours], default=0)
    rest = models.IntegerField('Остаток отпуска, дни', null=True, blank=True, default=None)
    employment = models.DateField('Дата приема на работу')

    def get_full_name(self):
        if self.lastname and self.firstname:
            if self.middlename:
                full_name = self.lastname + ' ' + self.firstname + ' ' + self.middlename
            else:
                full_name = self.lastname + ' ' + self.firstname
        else:
            full_name = self.email
        return full_name

    @property
    def is_staff(self):
        return self.is_admin

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = 'Сотрудника'
        verbose_name_plural = 'Сотрудники'

class Period(models.Model):
    begin_date = models.DateField('Дата начала')
    end_date = models.DateField('Дата завершения')
    is_close = models.BooleanField('Закрыть период', default=False)
    reported = models.BooleanField('Отчет отправлен', default=False)

    class Meta:
        ordering = ['begin_date']

    def begin_period(self):
        return self.begin_date >= timezone.now() - datetime.timedelta(days=1)

    begin_period.admin_order_field = 'begin_date'

    def end_period(self):
        return self.end_date >= timezone.now() - datetime.timedelta(days=1)

class Report(models.Model):
    user = models.ForeignKey(ExtUser)
    period = models.ForeignKey(Period)
    work = models.IntegerField('Работал, ч', null=True, blank=True, validators=[validate_hours])
    ill = models.IntegerField('Болел, ч', null=True, blank=True, validators=[validate_hours], default=0)
    holiday = models.IntegerField('Отпуск, ч', null=True, blank=True, validators=[validate_hours], default=0)
    do = models.TextField('Что сделал', null=True, blank=True)
    do_html = models.TextField('Что сделал (html)', null=True, blank=True)
    last_notice = models.DateField('Последнее извещение', null=True, blank=True)
    mark = models.IntegerField('Удовольствие от работы (от 1 до 10)', null=True, blank=True, default=5)

class Setting(models.Model):
    key = models.CharField(max_length=40, null=False, blank=False)
    value = models.CharField(max_length=100, null=False, blank=False)
    #domen_name, relative_date, period_length, notice_norm, notice_long, notice_time, admin_email, export_emails, holiday_days

class Holidays(models.Model):
    user = models.ForeignKey(ExtUser)
    rest = models.IntegerField('Остаток с прошлого года, ч.', null=True, blank=True, default=0)
    spent = models.IntegerField('Отпуск за текущий период', null=True, blank=True, default=20)
    year = models.IntegerField('Год')
