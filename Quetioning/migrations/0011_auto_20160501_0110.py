# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-30 18:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quetioning', '0010_auto_20160501_0108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='ill',
            field=models.IntegerField(null=True, verbose_name='Ill'),
        ),
    ]
