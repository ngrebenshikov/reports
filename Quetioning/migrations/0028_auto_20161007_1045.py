# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-07 03:45
from __future__ import unicode_literals

import Quetioning.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quetioning', '0027_auto_20161007_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='extuser',
            name='rest',
            field=models.IntegerField(blank=True, default=0, null=True, validators=[Quetioning.models.validate_hours], verbose_name='Остаток отпуска, дни'),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='last_year_holiday',
            field=models.IntegerField(blank=True, default=0, null=True, validators=[Quetioning.models.validate_hours], verbose_name='Остаток отпуска c прошлых лет, дни'),
        ),
    ]
