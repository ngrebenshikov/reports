# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-07 02:19
from __future__ import unicode_literals

import Quetioning.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quetioning', '0025_auto_20161004_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='extuser',
            name='last_year_holiday',
            field=models.IntegerField(blank=True, null=True, validators=[Quetioning.models.validate_hours], verbose_name='Остаток отпуска, дни'),
        ),
        migrations.AlterField(
            model_name='report',
            name='do_html',
            field=models.TextField(blank=True, null=True, verbose_name='Что сделал (html)'),
        ),
    ]
