from django_cron import CronJobBase, Schedule
from django.shortcuts import get_object_or_404
from datetime import date, datetime, timedelta
from Quetioning.models import ExtUser, Period, Report, Setting
from calendar import monthrange
from django.core.mail import send_mail
from django.template.loader import render_to_string
from urllib import request
from Quetioning.functions import func

class OpenPeriod(CronJobBase): # открытие периода
    RUN_EVERY_MINS = 0.1 # every 12 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'quest.open_period'    # a unique code

    def do(self):
        if Period.objects.all():
            relative_date = int(get_object_or_404(Setting, key='relative_date').value)
            period_length = int(get_object_or_404(Setting, key='period_length').value)
            first = Period.objects.order_by('-end_date')[0].end_date + timedelta(days=1)
            if first.day != 1:
                while date.weekday(first) != 0:
                    first += timedelta(days=1)
                end = first + timedelta(days=period_length-1)
            else:
                end = first + timedelta(days=period_length-1)
                while date.weekday(end) != 6:
                    end += timedelta(days=1)
            open_date = first + timedelta(days=relative_date)
            if  first.month != (end + timedelta(days=period_length)).month:
                end = date(first.year, first.month, monthrange(first.year,first.month)[1])
            if date.today() >= open_date:
                period = Period(
                    begin_date= first,
                    end_date= end
                )
                period.save()
                func.create_reports(period)

class SendNotices(CronJobBase): # оповещения
    RUN_EVERY_MINS = 1 # every 12 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'quest.send_notices'    # a unique code

    def do(self):
        if Period.objects.all():
            notice_norm = int(get_object_or_404(Setting, key='notice_norm').value)
            notice_long = int(get_object_or_404(Setting, key='notice_long').value)
            notice_time = get_object_or_404(Setting, key='notice_time').value
            period_length = int(get_object_or_404(Setting, key='period_length').value)
            time = datetime.strptime(notice_time, '%H:%M')
            periods = Period.objects.filter(is_close=False)
            for period in periods:
                if (period.end_date - period.begin_date).days + 1 != period_length and period.begin_date.day != 1:
                	date = period.end_date + timedelta(days=notice_long*(-1))
                else:
                    date = period.end_date + timedelta(days=notice_norm*(-1))
                notice = datetime(date.year, date.month, date.day, time.hour, time.minute)
                if datetime.now() >= notice:
                    func.auto_notices(period, date)

class ExtraNotices(CronJobBase): # дополнительные оповещения
    RUN_EVERY_MINS = 0.1 # every 12 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'quest.extra_notices'    # a unique code

    def do(self):
        if Period.objects.all():
            notice_time = get_object_or_404(Setting, key='notice_time').value
            period_length = int(get_object_or_404(Setting, key='period_length').value)
            time = datetime.strptime(notice_time, '%H:%M')
            periods = Period.objects.filter(is_close=False)
            for period in periods:
                if (period.end_date - period.begin_date).days + 1 != period_length and period.begin_date.day != 1: #контрольные периоды
                    dates = []
                    base_date = datetime(period.end_date.year, period.end_date.month, period.end_date.day, time.hour, time.minute)
                    dates.append(base_date + timedelta(days=-4))
                    dates.append(base_date + timedelta(days=-3))
                    dates.append(base_date + timedelta(days=-2))
                    if datetime.now() >= dates[0] and datetime.now() < dates[1]:
                        func.auto_notices(period,dates[0])
                    if datetime.now() >= dates[1]:
                        func.auto_notices(period,dates[1])
                    if datetime.now() >= dates[2]:
                        if not period.reported:
                            func.report_email(period)
                else: #обычные периоды
                    dates = []
                    base_date = datetime(period.end_date.year, period.end_date.month, period.end_date.day, time.hour, time.minute)
                    dates.append(base_date + timedelta(days=1))
                    dates.append(base_date + timedelta(days=3))
                    dates.append(base_date + timedelta(days=4))
                    if datetime.now() >= dates[0] and datetime.now() < dates[1]:
                        func.auto_notices(period,dates[0])
                    if datetime.now() >= dates[1]:
                        func.auto_notices(period,dates[1])
                    if datetime.now() >= dates[2]:
                        if not period.reported:
                            func.report_email(period)
