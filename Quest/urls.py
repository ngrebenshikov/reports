# coding: utf-8
"""Quest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include#я поправил, patterns
from django.contrib import admin
from Quetioning import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    #urls for admin
    url(r'^admin/', include('Quetioning.urls')),
    #url(r'^admin/', admin.site.urls), я поправил
    # urls for users
    url(r'^$', views.main, name='n_main'),
    url(r'my_report/(?P<pk>[0-9]+)/$', views.my_report, name='my_report'),
    url(r'export/$', views.export, name='user_export'),
    url(r'holiday/$', views.holidays, name='user_holiday'),
    url(r'xml/(?P<begin>[.0-9]+)-(?P<end>[.0-9]+)-(?P<id_user>[0-9]+)/$', views.xml, name='user_xml'),
    # urls for auth
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url('^', include('django.contrib.auth.urls')),
]
